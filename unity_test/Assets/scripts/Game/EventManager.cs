﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Test
{
    public enum GameEvents {START, WIN, GAMEOVER, PLAYERTOOSMALL, PATHCLEAN, RESTART};
    public class EventManager 
    {
        #region public methods
        #endregion
        #region private fields
        static EventManager _ref;
        Dictionary<GameEvents, Action<object>> _events = new Dictionary<GameEvents, Action<object>>();

        Dictionary<GameEvents, Func<object, object>> _eventsRetParam = new Dictionary<GameEvents, Func<object,object>>();

        #endregion
        #region private methods
        EventManager()
        {

        }
        #endregion


        #region public methods
        static public EventManager Instance()
        {
            if (_ref == null)
            {
                _ref = new EventManager();
            }

            return _ref;
        }
        public void AddListener(GameEvents event_, Action<object> listener_)
        {
            if (_events.ContainsKey(event_))
            {
                _events[event_] += listener_;
                return;
            }
            _events.Add(event_, listener_);
        }

        public void Event(GameEvents events_, object param_)
        {
            if (_events.ContainsKey(events_))
            {
                _events[events_].Invoke(param_);
            }
        }

        public void AddListenerRetParam(GameEvents event_, Func<object,object> listener_)
        {
            if (_eventsRetParam.ContainsKey(event_))
            {
                _eventsRetParam[event_] += listener_;
                return;
            }
            _eventsRetParam.Add(event_, listener_);
        }

        public object EventRetParam(GameEvents events_, object param_)
        {
            if (_eventsRetParam.ContainsKey(events_))
            {
                return _eventsRetParam[events_].Invoke(param_);
            }
            return null;
        }
        public void Init()
        {
            _events.Clear();
        }
        
        #endregion
    }
}