﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{

    public class Bullet : MonoBehaviour
    {
        #region private fields

        private bool _isStop = true;
        private List<GameObject> _barriers = new List<GameObject>();

        private BulletData _data;
        #endregion

        #region private methods
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (GameState.Instance().State != GameStates.PLAY) return;
            if (!_isStop)
            {
                transform.position += transform.forward * Time.deltaTime * _data.Speed;
                _barriers.Clear();
            }
            else
            //по хорошему на препятствия тоже нужно было бы повесить скрип
            // и передавать туда урон. Так позже может понадобиться делать
            // препятстия переживающие не один, а несколько взрывов
            if (_barriers.Count > 0)
            {
                foreach( var elem in _barriers)
                {
                    Destroy(elem);
                }
                Destroy(gameObject);
            }
            
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Barrier")
            {
                _isStop = true;
            }
        }
        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.tag == "Barrier")
            {
                _barriers.Add(other.gameObject);
            }
        }
        #endregion
        #region public properties
        public void Init(BulletData data_)
        {
            SphereCollider[] colliders = GetComponents<SphereCollider>();

            _data = data_;
            foreach( SphereCollider collider in colliders)
            {
                if (collider.isTrigger == true)
                {
                    collider.radius = _data.Radius;
                    break;
                }
            }

            IsStop = true;
        }

        public bool IsStop
        {
            set { _isStop = value; }
            get { return _isStop; }
        }
        #endregion
    }
}
