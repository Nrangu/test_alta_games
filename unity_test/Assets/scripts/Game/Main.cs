﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


namespace Test
{
    public class Main : MonoBehaviour
    {
        #region private fields
        static Main _ref;
        #endregion

        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;

            }
            else
            {
                Destroy(gameObject);
            }
            //DontDestroyOnLoad(gameObject);

 



        }
      


        void Init()
        {
            EventManager.Instance().Init();
            GameState.Instance().Init();

            GameObject menuStart = FindObjectOfType<MenuStart>().gameObject;
            menuStart.SetActive(false);

            GameObject menuWin = FindObjectOfType<MenuWin>().gameObject;
            menuWin.SetActive(false);

            GameObject menuPause = FindObjectOfType<MenuPause>().gameObject;
            menuPause.SetActive(false);

            GameObject menuGameOver = FindObjectOfType<MenuGameOver>().gameObject;
            menuGameOver.SetActive(false);

            GameState.Instance().OnStart += () => { menuStart.SetActive(true); };
            GameState.Instance().OnGameOver += () => { menuGameOver.SetActive(true); };
            GameState.Instance().OnWin += () => { menuWin.SetActive(true); };

            EventManager.Instance().AddListener(GameEvents.PLAYERTOOSMALL, (object param) => { GameState.Instance().State = GameStates.GAMEOVER; });
            EventManager.Instance().AddListener(GameEvents.PATHCLEAN, (object param) => {GameState.Instance().State = GameStates.WIN ; });
            EventManager.Instance().AddListener(GameEvents.RESTART, (object param) => {
                                                                                    SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
                                                                                    SceneManager.LoadScene(0);
                                                                                    });

            Player.Instance().Init(Level.Instance().LevelData.PlayerData, Level.Instance().LevelData.BulletData);


            


        }
        
        private void Start()
        {
            GameState.Instance().State = GameStates.START;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
 
        private void OnEnable()
        {
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }

        private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            Init();

        }

        #endregion
        #region public methods
 
        #endregion
        #region public properties
        #endregion
        #region public methods
        static public Main Instance()
        {
            return _ref;
        }
        #endregion

    }
}
