﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{

    public class Path : MonoBehaviour
    {
        #region private fields;
        private bool _pathClean = false;
        #endregion
        #region private methods
        private void Awake()
        {
            _pathClean = false;
        }
        private void FixedUpdate()
        {
                        
            if (GameState.Instance().State != GameStates.PLAY) return;

            if (_pathClean)
            {
                EventManager.Instance().Event(GameEvents.PATHCLEAN, null);
            }
            _pathClean = true;

            //    
            
        }
        
        private void OnTriggerStay(Collider other)
        {
            if (GameState.Instance().State != GameStates.PLAY) return;
            if (other.gameObject.tag == "Barrier")
            {
                _pathClean = false;
            }
            
        }
        #endregion
    }
}