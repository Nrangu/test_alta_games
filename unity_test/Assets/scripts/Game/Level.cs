﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{

    public class Level : MonoBehaviour
    {
        #region private fields
        [SerializeField] LevelData _data;
        static private Level _ref;
        #endregion

        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion
        #region public methods
        static public Level Instance()
        {
            return _ref;
        }

        public LevelData LevelData
        {
            get
            {
                return _data;
            }
        }
        #endregion
    }
}