﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
    [CreateAssetMenu(menuName ="Test/Bullet data",fileName ="BulletData")]
    public class BulletData : ScriptableObject
    {
        #region private fields

        [Tooltip("скорость передвижения выстрела")]
        [SerializeField] private float _speed;

        [Tooltip("радиус обнаружения препятствий")]
        [SerializeField] private float _radius;

        #endregion
        #region public methods, properties
        public float Speed
        {
            get
            {
                return _speed;
            }
        }


        public float Radius
        {
            get
            {
                return _radius;
            }
        }
        #endregion
    }
}