﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
    [CreateAssetMenu(menuName ="Test/Level data",fileName ="LevelData")]
    public class LevelData : ScriptableObject
    {
        #region private fields

        [SerializeField] private PlayerData _playerData;
        [SerializeField] private BulletData _bulletData;
        
        #endregion

        #region public methods, properties

        public PlayerData PlayerData
        {
            get
            {
                return _playerData;
            }
        }

        public BulletData BulletData
        {
            get
            {
                return _bulletData;
            }
        }

        #endregion
    }
}