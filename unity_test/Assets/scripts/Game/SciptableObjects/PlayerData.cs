﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
    [CreateAssetMenu(menuName ="Test/Player data", fileName ="PlayerData")]
    public class PlayerData : ScriptableObject
    {
        #region private fields

        [Tooltip("Начальные размер сферы игрока")]
        [SerializeField] private float _startScale;

        [Tooltip("Насколько сфера уменьшается за секунду")]
        [SerializeField] private float _deltaScale;

        [Tooltip("минимальный размер при котором проигрыш")]
        [SerializeField] private float _minScale;

        [Tooltip("префаб объекта которым стреляет игрок")]
        [SerializeField] private GameObject _bulletPrefab;

        #endregion

        #region public methods, properties
        public float StartScale
        {
            get
            {
                return _startScale;
            }
        }

        public float DeltaScale
        {
            get
            {
                return _deltaScale;
            }
        }

        public float MinScale
        {
            get
            {
                return _minScale;
            }
        }

        public GameObject BulletPrefab
        {
            get
            {
                return _bulletPrefab;
            }
        }
        #endregion
    }
}