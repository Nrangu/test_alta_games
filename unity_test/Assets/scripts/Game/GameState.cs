﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{

    public enum GameStates {NOTINIT, START, PLAY,  PAUSE, GAMEOVER, WIN };

    public class GameState
    {
        #region private fields
        GameStates _state = GameStates.NOTINIT;
        static GameState _ref;
        #endregion

        #region public fields
        public event Action OnStart = () => { };
        public event Action OnGameOver = () => { };
        public event Action OnWin = () => { };
        #endregion
        #region public methods, properties
        public void Init()
        {
            OnStart = () => { };
            OnGameOver = () => { };
            OnWin = () => { };
        }
        public GameStates State
        {
            get
            {
                return _state;
            }

            set
            {
                _state = value;
                SetState(_state);
            }
        }
        static public GameState Instance()
        {
            if (_ref == null)
            {
                _ref = new GameState();
            }

            return _ref;
        }
        #endregion
        #region private methods
        GameState()
        {

        }
        void SetState(GameStates state_)
        {
            switch (state_)
            {
                case GameStates.START:
                    OnStart();
                    break;
                case GameStates.GAMEOVER:
                    OnGameOver();
                    break;
                case GameStates.WIN:
                    OnWin();
                    break;
            }
        }
        #endregion
    }
}
