﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Test
{
    public class MenuPause : MenuGameOver
    {
        #region public methods
        public void ContinueClick()
        {
            gameObject.SetActive(false);
            GameState.Instance().State = GameStates.PLAY;
            
        }
        #endregion
        #region private methods
        private void OnEnable()
        {
            GameState.Instance().State = GameStates.PAUSE;
        }
        #endregion
    }
}