﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Test
{
    public class MenuGameOver : MonoBehaviour
    {
        #region public methods
        public void RestartClick()
        {
            EventManager.Instance().Event(GameEvents.RESTART, null);
        }
        public void ExitClick()
        {
            Application.Quit();
        }
        #endregion
        #region private methods

        #endregion
    }
}