﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{

    public class Player : MonoBehaviour
    {
        
        #region private fields
        [SerializeField] private Transform _shootPoint;

        static private Player _ref = null;

        private GameObject _bulletPrefab;
        private float _scale = 1.5f;
        private float _seconds = 2f;
        private float _speed = 0.3f;
        private float _minSize = 0.15f;

        private PlayerData _data;
        private BulletData _bulletData = null;

        private Vector3 _scalePlayer;
        private Vector3 _scaleBullet;
        private bool isMouseDown = false;
        private GameObject _bullet = null;
        /*


        private bool _win = false;
        private float _countTime = 0;
        private float _deltaScale = 0;
        private float speed = 0.4f;
        private bool _onDown = false;
        private bool _onUp = false;


        private bool _isStop = true;
        */
        #endregion
        #region private methods
        private void Awake()
        {
            if( _ref == null)
            {
                _ref = this;
            }
            else
            {
                Destroy(gameObject);
            }
            /*
            _scale = gameObject.transform.localScale.x;
            _deltaScale = _scale / _seconds;
            gameObject.transform.localScale = new Vector3( _scale, _scale, _scale );
            */
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (GameState.Instance().State != GameStates.PLAY) return;
            if (isMouseDown)
            {
                _scaleBullet.x = _scaleBullet.x + (Time.deltaTime * _data.DeltaScale);
                _scaleBullet.y = _scaleBullet.y + (Time.deltaTime * _data.DeltaScale);
                _scaleBullet.z = _scaleBullet.z + (Time.deltaTime * _data.DeltaScale);

                _bullet.transform.localScale = _scaleBullet;

                _scalePlayer.x = _scalePlayer.x - (Time.deltaTime * _data.DeltaScale);
                _scalePlayer.y = _scalePlayer.y - (Time.deltaTime * _data.DeltaScale);
                _scalePlayer.z = _scalePlayer.z - (Time.deltaTime * _data.DeltaScale);

                if (_scalePlayer.x <= _data.DeltaScale)
                {
                    EventManager.Instance().Event(GameEvents.PLAYERTOOSMALL, null);

                }
                gameObject.transform.localScale = _scalePlayer;
            }

        }
        private void OnMouseDown()
        {
            if (GameState.Instance().State != GameStates.PLAY) return;

            if (_bullet == null)
            {
                _bullet = Instantiate(_bulletPrefab, _shootPoint.position, _shootPoint.rotation);
                _bullet.GetComponent<Bullet>().Init(_bulletData);
                _scaleBullet = new Vector3(0, 0, 0);
                _bullet.transform.localScale = _scaleBullet;
                isMouseDown = true;
            }
        }

        private void OnMouseUp()
        {
            if (GameState.Instance().State != GameStates.PLAY) return;
            if (_bullet != null)
            {
                _bullet.GetComponent<Bullet>().IsStop = false;
                isMouseDown = false;
                _bullet = null;
            }

        }

        private void OnCollisionEnter(Collision collision)
        {
            /*
            if (collision.gameObject.tag == "Finish")
            {
                _win = true;
                Debug.Log("Finish");
                Destroy(this);
            } 
            */
        }


        #endregion
        #region public properties
        static public Player Instance()
        {
            return _ref;
        }

        public void Init(PlayerData data_, BulletData bulletData_)
        {
            _data = data_;
            _bulletData = bulletData_;
            _bulletPrefab = _data.BulletPrefab;
            _bullet = null;
            _scalePlayer = new Vector3(_data.StartScale, _data.StartScale, _data.StartScale);
            gameObject.transform.localScale = _scalePlayer;
        }
        /*
        public void Go()
        {
            IsStop = true;
        }
        public bool IsStop
        {
            get { return _isStop; }
            set { _isStop = value; }
        }
        */
        #endregion
    }
}